import { createRouter, createWebHistory } from 'vue-router'

import Login from '../views/login.vue'
import Home from '../views/home.vue'
import SysMenu from '../views/sysMenu.vue'
import SysSetting from '../views/sysSetting.vue'

export default new createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path:'/home',
            name:'home',
            component: Home,
            children:[
                {
                    path: '/sysMenu',
                    name:'菜单管理',
                    component: SysMenu
                },
                {
                    path: '/sysSetting',
                    name:'系统设置',
                    component: SysSetting
                }
            ]
        }
    ]
})
